import { combineReducers } from "redux";
import { contactReducer } from "./contact_reducer";

const rootReducer = combineReducers({
  contactList: contactReducer
});

export default rootReducer;
