import React from "react";
import { Grid, Dropdown, Input, Button, Label } from "semantic-ui-react";
import styles from "../assets/styles";
import countries from "country-list";
const AddressBookView = ({
  contactList,
  selectContact,
  selectedContact,
  handleFieldChange,
  handleView,
  view,
  saveContact,
  editContact,
  deleteContact,
  message
}) => {
  return (
    <Grid.Column width={10} textAlign="center">
      <Button
        circular
        icon="check"
        style={styles.checkButton}
        onClick={() =>
          view === "add"
            ? saveContact(selectedContact)
            : editContact(selectedContact)
        }
      />
      <p style={styles.editOrAddButton}>
        {view === "add" ? "Add new" : "Edit"} contact
      </p>
      <Label circular size="massive" style={styles.bigCircle}>
        <p style={styles.pBigCircle}>
          {selectedContact.firstName || selectedContact.lastName
            ? `${selectedContact.firstName
                .charAt(0)
                .toUpperCase()}${selectedContact.lastName
                .charAt(0)
                .toUpperCase()}`
            : "NC"}
        </p>
      </Label>
      <p style={styles.detailName}>
        {selectedContact.firstName || selectedContact.lastName
          ? `${selectedContact.firstName} ${selectedContact.lastName}`
          : "New contact"}
      </p>
      <Grid style={styles.detailGrid}>
        <Grid.Row columns={2}>
          <Grid.Column>
            <p> First Name:</p>
            <Input
              size="mini"
              placeholder="Search..."
              onChange={event =>
                handleFieldChange("firstName", event.target.value)
              }
              style={styles.editInput}
              value={selectedContact.firstName}
            />
          </Grid.Column>
          <Grid.Column style={styles.detailRightColum}>
            <p>Last Name:</p>
            <Input
              size="mini"
              placeholder="Search..."
              onChange={event =>
                handleFieldChange("lastName", event.target.value)
              }
              style={styles.editInput}
              value={selectedContact.lastName}
            />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row columns={2}>
          <Grid.Column>
            <p> Email:</p>
            <Input
              type="text"
              size="mini"
              placeholder="Search..."
              onChange={event => handleFieldChange("email", event.target.value)}
              style={styles.editInput}
              value={selectedContact.email}
            />
          </Grid.Column>
          <Grid.Column style={styles.detailRightColum}>
            <p>Country:</p>
            <Dropdown
              placeholder="Select Country"
              fluid
              selection
              upward
              options={countries()
                .getData()
                .map(({ code, name }) => ({ text: name, value: name }))}
              style={styles.dropdownEdit}
              onChange={(event, data) =>
                handleFieldChange("country", data.value)
              }
              value={selectedContact.country}
            />
          </Grid.Column>
        </Grid.Row>
        {message && <p style={styles.errors}>{message}</p>}
      </Grid>
      {view !== "add" && (
        <Button
          circular
          icon="trash"
          style={styles.deleteButton}
          onClick={() => deleteContact(selectedContact)}
        />
      )}
    </Grid.Column>
  );
};

export default AddressBookView;
